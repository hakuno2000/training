package com.training.demo.service;

import com.training.demo.entity.Item;
import com.training.demo.entity.Order;
import com.training.demo.entity.OrderDetail;
import com.training.demo.repo.ItemRepo;
import com.training.demo.repo.OrderDetailRepo;
import com.training.demo.repo.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl implements OrderDetailService{
    @Autowired
    private OrderDetailRepo orderDetailRepo;
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private ItemRepo itemRepo;

    @Override
    public OrderDetail save(Long orderId, Long itemId, Long quantity, OrderDetail orderDetail) {
        Order order = orderRepo.findOrderById(orderId);
        if (order == null) return null;

        Item item = itemRepo.findItemById(itemId);
        if (item == null) return null;

        orderDetail.setItems(item);
        orderDetail.setOrder(order);
        orderDetail.setQuantity(quantity);
        return orderDetailRepo.save(orderDetail);
    }
}
