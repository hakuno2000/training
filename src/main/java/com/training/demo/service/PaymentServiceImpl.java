package com.training.demo.service;

import com.training.demo.entity.Order;
import com.training.demo.entity.OrderDetail;
import com.training.demo.entity.Payment;
import com.training.demo.repo.OrderRepo;
import com.training.demo.repo.PaymentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PaymentServiceImpl implements PaymentService{
    @Autowired
    private PaymentRepo paymentRepo;
    @Autowired
    private OrderRepo orderRepo;

    @Override
    public Payment save(Long orderId, Long cash, Long credit, Payment payment) {
        Order temp = orderRepo.findOrderById(orderId);
        if (temp == null) return null;

        payment.setCash(cash);
        payment.setCredit(credit);

        Long basePrice = 0L;
        Long sellPrice = 0L;

        payment.setOrder(temp);
        Set<OrderDetail> orderedItems = payment.getOrder().getOrderDetails();
        for(OrderDetail orderedItem : orderedItems) {
            basePrice += orderedItem.getItems().getBasePrice() * orderedItem.getQuantity();
            sellPrice += orderedItem.getItems().getSellPrice() * orderedItem.getQuantity();
        }

        payment.setBasePrice(basePrice);
        payment.setSellPrice(sellPrice);

        if (sellPrice > cash + credit) return null;

        return paymentRepo.save(payment);
    }
}
