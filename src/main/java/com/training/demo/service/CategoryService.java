package com.training.demo.service;

import com.training.demo.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findCategoryById(Long id);
    Category save(Category category, Long shopId);
    void deleteById(Long id);
    List<Category> findCategoriesByShopId(Long shopId);
    List<Category> findCategoriesByShopIdAndNameContaining(Long shopId, String name);
    Category updateById(Category category, Long id);
}
