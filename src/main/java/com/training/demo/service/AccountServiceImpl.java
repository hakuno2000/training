package com.training.demo.service;

import com.training.demo.entity.Account;
import com.training.demo.repo.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService{
    @Autowired
    private AccountRepo accountRepo;

    @Override
    public List<Account> findAll() {
        return accountRepo.findAll();
    }

    @Override
    public Account findAccountById(Long id) {
        return accountRepo.findAccountById(id);
    }

    @Override
    public void save(Account account) {
        accountRepo.save(account);
    }

    @Override
    public void deleteById(Long id) {
        accountRepo.deleteById(id);
    }

    @Override
    public Account login(Account account) {
        Account matchedAccount = accountRepo.findAccountByUsername(account.getUsername());
        if (matchedAccount == null) return null;
        if (matchedAccount.getPassword().equals(account.getPassword())) return matchedAccount;
        return null;
    }
}
