package com.training.demo.service;

import com.training.demo.entity.Item;

import java.util.List;

public interface ItemService {
    List<Item> findAll();
    Item findItemById(Long id);
    Item save(Item item, Long categoryId);
    void deleteById(Long id);
    List<Item> findItemsByNameContaining(String name);
    Item updateById(Item item, Long id);
    List<Item> findItemsByShopId(Long shopId);
    List<Item> findItemsByShopIdAndNameContaining(Long shopId, String name);
}
