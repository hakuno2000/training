package com.training.demo.service;

import com.training.demo.entity.Category;
import com.training.demo.entity.Item;
import com.training.demo.entity.Shop;
import com.training.demo.repo.CategoryRepo;
import com.training.demo.repo.ItemRepo;
import com.training.demo.repo.ShopRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService{
    @Autowired
    private ItemRepo itemRepo;
    @Autowired
    private ShopRepo shopRepo;
    @Autowired
    private CategoryRepo categoryRepo;

    @Override
    public List<Item> findAll() {
        return itemRepo.findAll();
    }

    @Override
    public Item findItemById(Long id) {
        return itemRepo.findItemById(id);
    }

    @Override
    public Item save(Item item, Long categoryId) {
        Category category = categoryRepo.findCategoryById(categoryId);
        if (category == null) return null;
        item.setCategory(category);
        item.setShop(category.getShop());
        return itemRepo.save(item);
    }

    @Override
    public void deleteById(Long id) {
        itemRepo.deleteById(id);
    }

    @Override
    public List<Item> findItemsByNameContaining(String name) {
        return itemRepo.findItemsByNameContaining(name);
    }

    @Override
    public Item updateById(Item item, Long id) {
        Item temp = itemRepo.findItemById(id);
        if (temp == null) {
//            item.setId(id);
//            return itemRepo.save(item);
            return null;
        } else {
            if (item.getName() == null) item.setName(temp.getName());
            if (item.getBasePrice() == null) item.setBasePrice(temp.getBasePrice());
            if (item.getSellPrice() == null) item.setSellPrice(temp.getSellPrice());
            if (item.getDescription() == null) item.setDescription(temp.getDescription());
            if (item.getShop() == null) item.setShop(temp.getShop());
            if (item.getCategory() == null) item.setCategory(temp.getCategory());
            item.setId(id);
            return itemRepo.save(item);
        }
    }

    @Override
    public List<Item> findItemsByShopId(Long shopId) {
        return itemRepo.findItemsByShopId(shopId);
    }

    @Override
    public List<Item> findItemsByShopIdAndNameContaining(Long shopId, String name) {
        return itemRepo.findItemsByShopIdAndNameContaining(shopId, name);
    }
}
