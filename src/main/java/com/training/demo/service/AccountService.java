package com.training.demo.service;

import com.training.demo.entity.Account;

import java.util.List;

public interface AccountService {
    List<Account> findAll();
    Account findAccountById(Long id);
    void save(Account account);
    void deleteById(Long id);
    Account login(Account account);
}
