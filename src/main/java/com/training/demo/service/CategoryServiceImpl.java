package com.training.demo.service;

import com.training.demo.entity.Category;
import com.training.demo.entity.Shop;
import com.training.demo.repo.CategoryRepo;
import com.training.demo.repo.ShopRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{
    @Autowired
    private CategoryRepo categoryRepo;
    @Autowired
    private ShopRepo shopRepo;

    @Override
    public List<Category> findAll() {
        return categoryRepo.findAll();
    }

    @Override
    public Category findCategoryById(Long id) {
        return categoryRepo.findCategoryById(id);
    }

    @Override
    public Category save(Category category, Long shopId) {
        Shop shop = shopRepo.findShopById(shopId);
        if (shop == null) return null;
        category.setShop(shop);
        return categoryRepo.save(category);
    }

    @Override
    public void deleteById(Long id) {
        categoryRepo.deleteById(id);
    }

    @Override
    public List<Category> findCategoriesByShopId(Long shopId) {
        return categoryRepo.findCategoriesByShopId(shopId);
    }

    @Override
    public List<Category> findCategoriesByShopIdAndNameContaining(Long shopId, String name) {
        return categoryRepo.findCategoriesByShopIdAndNameContaining(shopId, name);
    }

    @Override
    public Category updateById(Category category, Long id) {
        Category temp = categoryRepo.findCategoryById(id);
        if (temp == null) {
//            category.setId(id);
//            return categoryRepo.save(category);
            return null;
        } else {
            if (category.getName() == null) category.setName(category.getName());
            if (category.getDescription() == null) category.setDescription(category.getDescription());
            if (category.getShop() == null) category.setShop(temp.getShop());
            category.setId(id);
            return categoryRepo.save(category);
        }
    }
}
