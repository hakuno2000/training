package com.training.demo.service;

import com.training.demo.entity.Shop;
import com.training.demo.repo.ShopRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ShopServiceImpl implements ShopService{
    @Autowired
    private ShopRepo shopRepo;

    @Override
    public List<Shop> findAll() {
        return shopRepo.findAll();
    }

    @Override
    public Shop findShopById(Long id) {
        return shopRepo.findShopById(id);
    }

    @Override
    public void save(Shop shop) {
        shopRepo.save(shop);
    }

    @Override
    public void deleteById(Long id) {
        shopRepo.deleteById(id);
    }
}
