package com.training.demo.service;

import com.training.demo.entity.*;
import com.training.demo.repo.CategoryRepo;
import com.training.demo.repo.ItemRepo;
import com.training.demo.repo.OrderRepo;
import com.training.demo.repo.ShopRepo;
import com.training.demo.support.Sale;
import com.training.demo.support.SaleByDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private ShopRepo shopRepo;
    @Autowired
    private ItemRepo itemRepo;
    @Autowired
    private CategoryRepo categoryRepo;

    @Override
    public Order save(Long shopId, Order order) {
        Shop shop = shopRepo.findShopById(shopId);
        if (shop == null) return null;

        order.setDate(LocalDate.now());
        order.setShop(shop);
        return orderRepo.save(order);
    }

    @Override
    public Order findOrderById(Long id) {
        return orderRepo.findOrderById(id);
    }

    @Override
    public Sale showSalesByDate(LocalDate date) {
        Sale sale = new Sale();
        List<Order> orders = orderRepo.findOrdersByDate(date);

        Long quantity = 0L, basePrice = 0L, sellPrice = 0L, cash = 0L, credit = 0L;

        for (Order order : orders) {
            for (OrderDetail orderDetail : order.getOrderDetails()) {
                if (orderDetail.getQuantity() == null) continue;
                quantity += orderDetail.getQuantity();
            }
            if (order.getPayment() == null) continue;
            basePrice += order.getPayment().getBasePrice();
            sellPrice += order.getPayment().getSellPrice();
            cash += order.getPayment().getCash();
            credit += order.getPayment().getCredit();
        }

        sale.setTransaction((long) orders.size());
        sale.setQuantity(quantity);
        sale.setBasePrice(basePrice);
        sale.setSellPrice(sellPrice);
        sale.setCash(cash);
        sale.setCredit(credit);
        sale.setInvestment(basePrice);
        sale.setProfit(sellPrice - basePrice);

        return sale;
    }

    @Override
    public List<Sale> showSalesByDateInMonth(Month month) {
        LocalDate startDate = LocalDate.of(LocalDate.now().getYear(), month, 1);
        LocalDate endDate = startDate.plusMonths(1);

        List<Sale> saleByDates = new ArrayList<>();
        for(LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
            Sale saleByDate = this.showSalesByDate(date);
            saleByDate.setDate(date);
            saleByDates.add(saleByDate);
        }

        return saleByDates;
    }

    @Override
    public Sale showSalesByItem(LocalDate date, Long itemId) {
        Sale sale = new Sale();
        List<Order> orders = orderRepo.findOrdersByDate(date);

        Long quantity = 0L, basePrice = 0L, sellPrice = 0L;

        for (Order order : orders) {
            for (OrderDetail orderDetail : order.getOrderDetails()) {
                if (orderDetail.getQuantity() == null) continue;
                if (!orderDetail.getItems().getId().equals(itemId)) continue;
                quantity += orderDetail.getQuantity();
                basePrice += orderDetail.getItems().getBasePrice() * orderDetail.getQuantity();
                sellPrice += orderDetail.getItems().getSellPrice() * orderDetail.getQuantity();
            }
        }

        sale.setQuantity(quantity);
        sale.setBasePrice(basePrice);
        sale.setSellPrice(sellPrice);
        sale.setInvestment(basePrice);
        sale.setProfit(sellPrice - basePrice);

        return sale;
    }

    @Override
    public List<Sale> showSalesByItemInDate(LocalDate date) {
        List<Item> items = itemRepo.findAll();

        List<Sale> saleByItems = new ArrayList<>();
        for (Item item : items){
            Sale sale = this.showSalesByItem(date, item.getId());
            sale.setItem(item);
            sale.setDate(date);
            saleByItems.add(sale);
        }

        return saleByItems;
    }

    @Override
    public Sale showSalesByCategory(LocalDate date, Long categoryId) {
        Sale sale = new Sale();
        List<Order> orders = orderRepo.findOrdersByDate(date);

        Long quantity = 0L, basePrice = 0L, sellPrice = 0L;

        for (Order order : orders) {
            for (OrderDetail orderDetail : order.getOrderDetails()) {
                if (orderDetail.getQuantity() == null) continue;
                if (!orderDetail.getItems().getId().equals(categoryId)) continue;
                quantity += orderDetail.getQuantity();
                basePrice += orderDetail.getItems().getBasePrice() * orderDetail.getQuantity();
                sellPrice += orderDetail.getItems().getSellPrice() * orderDetail.getQuantity();
            }
        }

        sale.setQuantity(quantity);
        sale.setBasePrice(basePrice);
        sale.setSellPrice(sellPrice);
        sale.setInvestment(basePrice);
        sale.setProfit(sellPrice - basePrice);

        return sale;
    }

    @Override
    public List<Sale> showSalesByCategoryInDate(LocalDate date) {
        List<Category> categories = categoryRepo.findAll();

        List<Sale> saleByCategories = new ArrayList<>();
        for (Category category : categories){
            Sale sale = this.showSalesByCategory(date, category.getId());
            sale.setCategory(category);
            sale.setDate(date);
            saleByCategories.add(sale);
        }

        return saleByCategories;
    }
}
