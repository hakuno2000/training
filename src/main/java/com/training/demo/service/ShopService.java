package com.training.demo.service;

import com.training.demo.entity.Shop;

import java.util.List;

public interface ShopService {
    List<Shop> findAll();
    Shop findShopById(Long id);
    void save(Shop shop);
    void deleteById(Long id);
}
