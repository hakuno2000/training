package com.training.demo.service;

import com.training.demo.entity.Payment;

public interface PaymentService {
    Payment save(Long orderId, Long cash, Long credit, Payment payment);
}
