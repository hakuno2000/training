package com.training.demo.service;

import com.training.demo.entity.Order;
import com.training.demo.support.Sale;
import com.training.demo.support.SaleByDate;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

public interface OrderService {
    Order save(Long shopId, Order order);
    Order findOrderById(Long id);
    Sale showSalesByDate(LocalDate date);
    List<Sale> showSalesByDateInMonth(Month month);
    Sale showSalesByItem(LocalDate date, Long itemId);
    List<Sale> showSalesByItemInDate(LocalDate date);
    Sale showSalesByCategory(LocalDate date, Long categoryId);
    List<Sale> showSalesByCategoryInDate(LocalDate date);
}
