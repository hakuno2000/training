package com.training.demo.service;

import com.training.demo.entity.OrderDetail;

public interface OrderDetailService {
    OrderDetail save(Long orderId, Long itemId, Long quantity, OrderDetail orderDetail);
}
