package com.training.demo.support;

import com.training.demo.entity.Category;
import com.training.demo.entity.Item;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter
public class Sale {
    private Long transaction;
    private Long quantity;
    private Long basePrice;
    private Long sellPrice;
    private Long cash;
    private Long credit;
    private Long investment;
    private Long profit;

    private LocalDate date;
    private Item item;
    private Category category;
}
