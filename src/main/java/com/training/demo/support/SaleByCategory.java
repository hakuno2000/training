package com.training.demo.support;

import com.training.demo.entity.Category;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SaleByCategory extends Sale{
    private Category category;
}
