package com.training.demo.support;

import com.training.demo.entity.Item;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SaleByItem extends Sale {
    private Item item;
}
