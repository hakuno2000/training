package com.training.demo.repo;

import com.training.demo.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepo extends JpaRepository<Item, Long> {
    List<Item> findAll();
    Item findItemById(Long id);
    List<Item> findItemsByNameContaining(String name);
    List<Item> findItemsByShopId(Long shopId);
    List<Item> findItemsByShopIdAndNameContaining(Long shopId, String name);

//    @Query(value = "", nativeQuery = true)
//    List<SalesByItem> showSalesByItem();
}
