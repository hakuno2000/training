package com.training.demo.repo;

import com.training.demo.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {
    List<Category> findAll();
    Category findCategoryById(Long id);
//    List<Category> findCategoriesByNameContaining(String name);
    List<Category> findCategoriesByShopId(Long shopId);
    List<Category> findCategoriesByShopIdAndNameContaining(Long shopId, String name);
}
