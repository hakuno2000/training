package com.training.demo.repo;

import com.training.demo.entity.Shop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShopRepo extends JpaRepository<Shop, Long> {
    List<Shop> findAll();
    Shop findShopById(Long id);
}
