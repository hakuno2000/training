package com.training.demo.repo;

import com.training.demo.entity.Order;
import org.aspectj.weaver.ast.Or;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface OrderRepo extends JpaRepository<Order, Long> {
    Order findOrderById(Long id);
    List<Order> findOrdersByDate(LocalDate date);
}
