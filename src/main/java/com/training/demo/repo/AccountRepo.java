package com.training.demo.repo;

import com.training.demo.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepo extends JpaRepository<Account, Long> {
    List<Account> findAll();
    Account findAccountById(Long id);
    Account findAccountByUsername(String username);
}
