package com.training.demo.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter @Setter
@Entity
@Table(name = "payment")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonManagedReference(value = "payment_order")
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private Order order;

    @Column(name = "base_price", nullable = false)
    private Long basePrice;

    @Column(name = "sell_price", nullable =  false)
    private Long sellPrice;

    @Column(name = "cash", nullable = false)
    private Long cash;

    @Column(name = "credit", nullable = false)
    private Long credit;
}
