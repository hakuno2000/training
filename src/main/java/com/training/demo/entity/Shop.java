package com.training.demo.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter @Setter
@Entity
@Table(name = "shops")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @JsonBackReference(value = "account_shop")
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;

    @JsonManagedReference(value = "shop_category")
    @OneToMany(mappedBy = "shop")
    private Set<Category> categories;

    @JsonManagedReference(value = "shop_item")
    @OneToMany(mappedBy = "shop")
    private Set<Item> items;

    @JsonManagedReference(value = "order_shop")
    @OneToMany(mappedBy = "shop")
    private Set<Order> orders;
}
