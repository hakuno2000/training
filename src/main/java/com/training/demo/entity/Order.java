package com.training.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter @Setter
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonBackReference(value = "order_shop")
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "shop_id", referencedColumnName = "id")
    private Shop shop;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @JsonManagedReference(value = "order_orderdetail")
    @OneToMany(mappedBy = "order")
    private Set<OrderDetail> orderDetails;

    @JsonBackReference(value = "payment_order")
    @OneToOne(mappedBy = "order")
    private Payment payment;
}
