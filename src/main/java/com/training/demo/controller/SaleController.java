package com.training.demo.controller;

import com.training.demo.entity.Order;
import com.training.demo.entity.OrderDetail;
import com.training.demo.service.OrderService;
import com.training.demo.support.Sale;
import com.training.demo.support.SaleByDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/sale")
public class SaleController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/date")
    public List<Sale> showSalesByDateInCurrentMonth() {
        return orderService.showSalesByDateInMonth(LocalDate.now().getMonth());
    }

    @GetMapping("/date/{month}")
    public List<Sale> showSalesByDateInMonth(@PathVariable("month")Month month) {
        return orderService.showSalesByDateInMonth(month);
    }

    @GetMapping("/item")
    public List<Sale> showSalesByIteminCurrentDate() {
        return orderService.showSalesByItemInDate(LocalDate.now());
    }

    @GetMapping("/item/{date}")
    public List<Sale> showSalesByIteminDate(@PathVariable("date") LocalDate date) {
        return orderService.showSalesByItemInDate(date);
    }

    @GetMapping("/category")
    public List<Sale> showSalesByCategoryinCurrentDate() {
        return orderService.showSalesByCategoryInDate(LocalDate.now());
    }

    @GetMapping("/category/{date}")
    public List<Sale> showSalesByCategoryinDate(@PathVariable("date") LocalDate date) {
        return orderService.showSalesByCategoryInDate(date);
    }
}
