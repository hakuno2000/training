package com.training.demo.controller;

import com.training.demo.entity.Payment;
import com.training.demo.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @PostMapping("{orderId}")
    public ResponseEntity<Object> payOrder(@PathVariable("orderId") Long orderId, @RequestParam("cash") Long cash, @RequestParam("credit") Long credit) {
        Payment temp = paymentService.save(orderId, cash, credit, new Payment());
        if (temp == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(temp);
    }
}
