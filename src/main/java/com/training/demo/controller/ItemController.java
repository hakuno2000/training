package com.training.demo.controller;

import com.training.demo.entity.Category;
import com.training.demo.entity.Item;
import com.training.demo.service.CategoryService;
import com.training.demo.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/item")
public class ItemController {
    @Autowired
    private ItemService itemService;

    @GetMapping("/{id}")
    public ResponseEntity<Object> showItem(@PathVariable("id") Long id) {
        Item item = itemService.findItemById(id);
        if (item == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(item);
    }

    @PutMapping("/{id}/update")
    public ResponseEntity<Object> updateById(@RequestBody Item item, @PathVariable("id") Long id){
        return ResponseEntity.ok(itemService.updateById(item, id));
    }

    @DeleteMapping("/{id}/delete")
    public void deleteItemById(@PathVariable("id") Long id) {
        itemService.deleteById(id);
    }
}
