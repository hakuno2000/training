package com.training.demo.controller;

import com.training.demo.entity.OrderDetail;
import com.training.demo.service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/order/detail")
public class OrderDetailController {
    @Autowired
    private OrderDetailService orderDetailService;

    @PostMapping("/create/")
    public ResponseEntity<Object> orderItem(@RequestParam("orderId") Long orderId, @RequestParam("itemId") Long itemId, @RequestParam("quantity") Long quantity) {
        OrderDetail temp = orderDetailService.save(orderId, itemId, quantity, new OrderDetail());
        if (temp == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(temp);
    }
}
