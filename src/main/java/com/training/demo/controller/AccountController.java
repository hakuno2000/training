package com.training.demo.controller;

import com.training.demo.entity.Account;
import com.training.demo.entity.Category;
import com.training.demo.entity.Shop;
import com.training.demo.repo.AccountRepo;
import com.training.demo.service.AccountService;
import com.training.demo.service.CategoryService;
import com.training.demo.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody Account account) {
        Account matchedAccount = accountService.login(account);
        if (matchedAccount == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(matchedAccount);
    }
}
