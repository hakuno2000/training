package com.training.demo.controller;

import com.training.demo.entity.Order;
import com.training.demo.entity.Shop;
import com.training.demo.service.OrderService;
import com.training.demo.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/{id}")
    public ResponseEntity<Object> showOrder(@PathVariable("id") Long id) {
        Order order = orderService.findOrderById(id);
        if (order == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(order);
    }

    @PostMapping("/{shopId}/create")
    public ResponseEntity<Object> createOrder(@PathVariable("shopId") Long shopId) {
        Order temp = orderService.save(shopId, new Order());
        if (temp == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(temp);
    }
}
