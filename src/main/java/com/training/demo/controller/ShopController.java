package com.training.demo.controller;

import com.training.demo.entity.Category;
import com.training.demo.entity.Item;
import com.training.demo.service.CategoryService;
import com.training.demo.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/shop")
public class ShopController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ItemService itemService;

    @GetMapping("/{shopId}/categories")
    public ResponseEntity<Object> showCategoriesByShop(@PathVariable("shopId") Long shopId) {
        List<Category> categories = categoryService.findCategoriesByShopId(shopId);
        if (categories.isEmpty()) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(categories);
    }

    @GetMapping("/{shopId}/categories/search")
    public ResponseEntity<Object> searchCategoriesByName(@PathVariable("shopId") Long shopId, @RequestParam("name") String name) {
        List<Category> categories = categoryService.findCategoriesByShopIdAndNameContaining(shopId, name);
        if (categories.isEmpty()) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(categories);
    }

    @PostMapping("/{shopId}/categories/add")
    public ResponseEntity<Object> addCategory(@PathVariable("shopId") Long shopId, @RequestBody Category category){
        Category temp = categoryService.save(category, shopId);
        if (temp == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(temp);
    }

    @GetMapping("/{shopId}/items")
    public ResponseEntity<Object> showItemsByShop(@PathVariable("shopId") Long shopId) {
        List<Item> items = itemService.findItemsByShopId(shopId);
        if (items == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(items);
    }

    @GetMapping("/{shopId}/items/search")
    public ResponseEntity<Object> searchItemsByName(@PathVariable("shopId") Long shopId, @RequestParam("name") String name) {
        List<Item> items = itemService.findItemsByShopIdAndNameContaining(shopId, name);
        if (items.isEmpty()) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(items);
    }
}
