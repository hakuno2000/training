package com.training.demo.controller;

import com.training.demo.entity.Category;
import com.training.demo.entity.Item;
import com.training.demo.service.CategoryService;
import com.training.demo.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ItemService itemService;

    @GetMapping("/{id}")
    public ResponseEntity<Object> showCategories(@PathVariable("id") Long id) {
        Category category = categoryService.findCategoryById(id);
        if (category == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(category);
    }

    @PutMapping("/{id}/update")
    public ResponseEntity<Object> updateById(@RequestBody Category category, @PathVariable("id") Long id){
        return ResponseEntity.ok(categoryService.updateById(category, id));
    }

    @DeleteMapping("/{id}/delete")
    public void deleteCategoryById(@PathVariable("id") Long id) {
        categoryService.deleteById(id);
    }

    @PostMapping("/{id}/add")
    public ResponseEntity<Object> addItem(@PathVariable("id") Long id, @RequestBody Item item) {
        Item temp = itemService.save(item, id);
        if (temp == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(item);
    }
}
